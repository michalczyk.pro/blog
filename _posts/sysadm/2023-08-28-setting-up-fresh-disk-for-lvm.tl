---
layout: default
title: Setting up fresh disk for LVM
date: 2023-08-28 22:58 +0200
tags: disks LVM
---
Format, partition and create file system.

* "SSD - Gentoo wiki":https://wiki.gentoo.org/wiki/SSD
* "SSD - Archlinux wiki":https://wiki.archlinux.org/title/Solid_state_drive


h2. System settings

Disable issuing discards for SSD drives, so LVM operations will be undoable:

{% highlight config caption=/etc/lvm/lvm.conf %}
issue_discards = 0
{% endhighlight %}


h2. Setting logical sector size

*Warning:* Changing sector size destroys all data.

List current logical and physical sector sizes for block devices:
{% highlight bash %}
$ lsblk -dt
NAME    ALIGNMENT MIN-IO OPT-IO PHY-SEC LOG-SEC ROTA SCHED       RQ-SIZE  RA WSAME
sda             0   4096      0    4096     512    1 mq-deadline       2 128    0B
nvme0n1         0    512      0     512     512    0 none           1023 128    0B
{% endhighlight %}

* for SATA drives: set logical sector size equal to physical (if supported)

{% highlight bash %}
# hdparm -I /dev/sda | grep 'Sector size:'
Logical  Sector size:                   512 bytes [ Supported: 512 4096 ]
Physical Sector size:                  4096 bytes
# hdparm --set-sector-size 4096 --please-destroy-my-drive  /dev/sda
{% endhighlight %}

* for NVMe drives: set logical sector size based on relative performance

{% highlight bash %}
# nvme id-ns -H /dev/nvme0n1 | grep 'Relative Performance'
LBA Format  0 : Metadata Size: 0   bytes - Data Size: 512 bytes - Relative Performance: 0x2 Good (in use)
LBA Format  1 : Metadata Size: 0   bytes - Data Size: 4096 bytes - Relative Performance: 0x1 Better
# nvme format --lbaf=1 /dev/nvme0n1
{% endhighlight %}

Further details: "Advanced format - Archlinux wiki":https://wiki.archlinux.org/title/Advanced_Format


h2. Partitioning and LVM setup

Parition alignment on 2MiB (equal to maximum possible Erase Block Size for
SSD/NVMe drives) equivalent to 4096 sectors 512B each/512 sectors 4KiB each.

{% highlight bash %}
# sfdisk /dev/nvme0n1
> label: gpt
{% endhighlight %}

Leave 2MiB of space at the beginning for parition table and (optionally)
bootloader.

For system drives, reserve 1022MiB EFI partition. Firmware updates
use EFI partition and may require >100MiB.

Remaining disk space is for LVM.  For non-system drives, LVM partition starts at
2MiB. Values given to @sfdisk@ are in *logical sectors* (as opposed to physical
sectors, as they're not necessarily same size) and example below is using 4KiB
logical sector size.

{% highlight bash %}
> 512, 261632, U, *
> 262144, , V,
> w
> q

# pvcreate --dataalignment 2M /dev/nvme0n1p2
# pvs /dev/nvme0n1p2 -o+pe_start
# vgcreate -s 1G vgdev /dev/nvme0n1p2
{% endhighlight %}

(verify that @pe_start@ is a multiple of alignment size).

If you want to extend existing VG, see "here":{% post_url
/sysadm/2023-09-15-lvm-cheat-sheet %}#convert-lv-to-raid1.


h2. File system setup and mouning

Create LV, optionally add redundancy/integrity layer ("details":{% post_url
/sysadm/2023-06-24-directly-attache-storage-with-redundancy-and-integrity-using-lvm-lvmraid
%}).

{% highlight bash %}
# lvcreate -L 2T -n backup --type raid1 --mirrors 1 vgdev
# lvconvert --raidintegrity y --raidintegrityblocksize 4096 --raidintegritymode bitmap /dev/vgdev/backup
{% endhighlight %}

For ext4 file systems, block sizes availavle on _x86_ architectures are from
4KiB to 64KiB.

Disable journaling.

For file systems on RAID devices other thatn RAID 1 (mirror), add _-E
stride=512,stripe_width=512,_ options. Stride size/stripe width is given in file
system block multiplies and should be aligned same as paritions (2MiB).

_-E discard_ can be omitted as it is the default.

{% highlight bash %}
# mkfs.ext4 -b 4096 -O ^has_journal /dev/vgdev/backup
{% endhighlight %}

Use following mount options in _/etc/fstab_:
* (for external disks) @noauto@
* @noatime,errors=remount-ro@
* (for SSD only) @nodiscard@ - discard on weekly basis using cron (make
sure you're using _anacron_ on desktop):

{% highlight bash caption=/etc/cron.weekly/fstrim %}
#!/bin/bash
/usr/bin/ionice -c 3 /sbin/fstrim -v -a
{% endhighlight %}


h2. Monitoring

h3. LV RAID health status

Make sure that cron messages are received, e.g. sent to proper e-mail address.

{% highlight bash caption=/etc/cron.hourly/lv-health-status %}
#!/bin/sh
lvs -o full_name,lv_health_status --separator ':' | tail -n+2 | egrep -v ':[[:space:]]*$'
{% endhighlight %}

TODO: monitoring smartd with cron task.
