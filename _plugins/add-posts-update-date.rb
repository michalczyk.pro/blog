Jekyll::Hooks.register :posts, :pre_render do |post|
  mtime = File.mtime post.path
  post.data['update_date'] = mtime unless post.data['date'].to_date == mtime.to_date
end
